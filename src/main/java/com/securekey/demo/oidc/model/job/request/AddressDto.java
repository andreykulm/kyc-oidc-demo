package com.securekey.demo.oidc.model.job.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AddressDto implements Serializable {

    private static final long serialVersionUID = 6394219046336524238L;
    private String streetAddress;
    private String locality;
    private String region;
    private String postalCode;
    private String country;
}