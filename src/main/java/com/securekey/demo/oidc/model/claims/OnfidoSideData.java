package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OnfidoSideData {

    private String applicantId;
    private OnfidoDocumentReport onfidoDocumentReport;
    private FacialSimilarity facialSimilarity;

    public OnfidoSideData updateFrom(OnfidoSideData from) {
        if (from == null)
            return this;
        this.setOnfidoDocumentReport(from.onfidoDocumentReport != null ? from.onfidoDocumentReport : this.onfidoDocumentReport);
        this.setFacialSimilarity(from.facialSimilarity != null ? from.facialSimilarity : this.facialSimilarity);
        return this;
    }
}
