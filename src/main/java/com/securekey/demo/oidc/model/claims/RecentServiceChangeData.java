package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class RecentServiceChangeData {

    private int responseCode;
    private RecentServiceChangeAge recentServiceChangeAge;
    private RecentServiceChangeFreq recentServiceChangeFreq;

}
