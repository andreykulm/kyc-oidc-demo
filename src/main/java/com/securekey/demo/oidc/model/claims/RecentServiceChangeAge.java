package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class RecentServiceChangeAge {

    private int simChangeAge;
    private int deviceChangeAge;
    private int phoneNumberChangeAge;

}
