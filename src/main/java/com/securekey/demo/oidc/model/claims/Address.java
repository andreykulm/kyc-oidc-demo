package com.securekey.demo.oidc.model.claims;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Address {

    private String streetAddress = "N/A";
    private String locality = "N/A";
    private String region = "N/A";
    private String postalCode = "N/A";
    private String country = "N/A";

}
