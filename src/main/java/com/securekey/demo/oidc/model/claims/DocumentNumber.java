package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DocumentNumber {

    public static final String DOCUMENT_NUMBER = "document_number";
    public static final String PASSPORT_NUMBER = "passport";
    public static final String PERSONAL_NUMBER = "personal_number";

    private String value;
    private String type;

}
