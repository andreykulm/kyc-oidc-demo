package com.securekey.demo.oidc.model.claims;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OnfidoDocumentReport {

    private String nationality;
    private String lastName;
    private String issuingCountry;
    private String gender;
    private String firstName;
    private String documentType;
    private List<DocumentNumber> documentNumbers;
    private String dateOfExpiry;
    private String dateOfBirth;
    private Address address = new Address();
//    private Extraction extraction;

}
