package com.securekey.demo.oidc.model.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetJobClaimResponse implements Serializable {

    private static final long serialVersionUID = 3295514173131910482L;
    private String id;

    private String clientTnxId;

    @JsonProperty("results")
    private ClaimDTO claim;

}
