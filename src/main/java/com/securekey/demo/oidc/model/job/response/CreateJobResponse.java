package com.securekey.demo.oidc.model.job.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CreateJobResponse implements Serializable {

    private static final long serialVersionUID = 4278314921845894606L;
    private String id;
    private String userHref;
    private JobDto job;
    private String permissionToken;

}
