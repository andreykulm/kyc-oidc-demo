package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RecentServiceChangeFreq {

    private int simChangeFreq;
    private int deviceChangeFreq;
    private int phoneNumberChangeFreq;

}
