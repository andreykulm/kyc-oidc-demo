package com.securekey.demo.oidc.model.get_job;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Results {
    @Valid
    private List<ServicesEnum> services = new ArrayList<ServicesEnum>();
    private DocumentType documentType;
    private Byte overallMatch;
    private Boolean telcoMatchResult;
    private ErrorMessage error;
}