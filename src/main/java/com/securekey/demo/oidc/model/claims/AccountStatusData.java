package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class AccountStatusData {

    private int responseCode;
    private String responseMessage;
    private String accountStatus;

}
