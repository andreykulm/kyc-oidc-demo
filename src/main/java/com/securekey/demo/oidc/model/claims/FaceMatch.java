package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public class FaceMatch {
    private String result;
    private Map<String, Double> properties = new HashMap<>();
}
