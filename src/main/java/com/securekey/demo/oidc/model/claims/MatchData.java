package com.securekey.demo.oidc.model.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class MatchData {

    @JsonProperty("requestID")
    private String requestId;

    @JsonProperty("requestTS")
    private double requestTs;

    private String lineOfBusiness;
    private boolean matchStatus;

    @JsonProperty("given_name_score")
    private byte givenNameScore;

    @JsonProperty("family_name_score")
    private byte familyNameScore;

    @JsonProperty("birthdate_score")
    private byte birthdateScore;

    @JsonProperty("postal_code_score")
    private byte postalCodeScore;

    private byte matchScore;

    private String matchThreshold;

    private int appStatusCode;

    private String appStatusDesc;

}
