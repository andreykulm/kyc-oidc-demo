package com.securekey.demo.oidc.model.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ClaimDTO {
    @JsonProperty("document")
    private OnfidoSideData onfidoSideData;

    @JsonProperty("match")
    private MatchData matchData;

    @JsonProperty("mobile_number")
    private EnstreamData enstreamData;
}
