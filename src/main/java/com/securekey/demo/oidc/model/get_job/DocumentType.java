package com.securekey.demo.oidc.model.get_job;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum DocumentType {
    PASSPORT("passport"),
    DRIVERS_LICENSE("drivers_license"),
    HEALTH_CARD("health_card"),
    NATIONAL_CARD("national_card");
    private String value;

    DocumentType(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static DocumentType fromValue(String text) {
        for (DocumentType b : DocumentType.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}