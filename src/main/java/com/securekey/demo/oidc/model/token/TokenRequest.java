package com.securekey.demo.oidc.model.token;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenRequest {

    private String code;

    private String grantType;

    private String redirectUri;

    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public String getGrantType() {
        return grantType;
    }

    @JsonProperty("grant_type")
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    @JsonProperty("redirect_uri")
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    @Override
    public String toString() {
        return "TokenRequest{" +
                "code='" + code + '\'' +
                ", grantType='" + grantType + '\'' +
                ", redirectUri='" + redirectUri + '\'' +
                '}';
    }
}
