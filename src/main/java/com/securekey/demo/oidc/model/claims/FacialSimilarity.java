package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public class FacialSimilarity {

    private static final String FACE_MATCH = "face_match";
    private static final String SCORE = "score";

    private String result;
    private Map<String, FaceMatch> breakdown = new HashMap<>();

    public Double getScore() {

        FaceMatch faceMatch = breakdown.get(FACE_MATCH);
        if (faceMatch == null) {
            return -1.0;
        }
        return faceMatch.getProperties().getOrDefault(SCORE, -1.0);
    }

    public FacialSimilarity setScore(double score) {
        breakdown.putIfAbsent(FACE_MATCH, new FaceMatch());
        FaceMatch faceMatch = breakdown.get(FACE_MATCH);
        faceMatch.getProperties().put(SCORE, score);
        return this;
    }
}
