package com.securekey.demo.oidc.model.job.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class JobDto implements Serializable {

    private static final long serialVersionUID = 162873200857179895L;
    private StateEnum state;
    private StatusEnum status;
    private String startTime;
    private String expiryTime;
    private String endTime;
    @JsonIgnore
    private String clientId;
    @JsonIgnore
    private String applicantId;
}