package com.securekey.demo.oidc.model.get_job;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ServicesEnum {
    DOCUMENT("document"),
    MATCHING("matching"),
    TELCO_MATCH("telco_match");
    private String value;

    ServicesEnum(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static ServicesEnum fromValue(String text) {
        for (ServicesEnum b : ServicesEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}