package com.securekey.demo.oidc.model.job.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Subject {
    private String givenName;
    private String middleName;
    private String familyName;
    private String phoneNumber;
    private AddressDto address;
    private String birthdate;
    private String email;

}