package com.securekey.demo.oidc.model.claims;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class EnstreamData {

    private RecentServiceChangeData recentServiceChangeData;
    private AccountStatusData accountStatusData;

    public boolean isAllData() {
        return recentServiceChangeData != null
                && accountStatusData != null;
    }

}
