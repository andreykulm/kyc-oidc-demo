package com.securekey.demo.oidc.model.get_job;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.securekey.demo.oidc.model.job.response.JobDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetJobResponse implements Serializable {

    private static final long serialVersionUID = 3210172003044838770L;
    private String id;
    private String clientTxnId;
    private JobDto job;
    private Results results;
    @JsonProperty("oauth2_token")
    private Oauth2Token oauth2Token;

}
