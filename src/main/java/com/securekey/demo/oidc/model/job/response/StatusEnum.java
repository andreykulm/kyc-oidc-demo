package com.securekey.demo.oidc.model.job.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusEnum {
    INITIATED("INITIATED"),
    USER_CONTACTED("USER_CONTACTED"),
    DOC_SCANNING("DOC_SCANNING"),
    DOC_PROCESSING("DOC_PROCESSING"),
    TELCO("TELCO"),
    MATCHING("MATCHING"),
    SUCCESS("SUCCESS"),
    CANCELED("CANCELED"),
    EXPIRED("EXPIRED");
    private String value;

    StatusEnum(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
        for (StatusEnum b : StatusEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}