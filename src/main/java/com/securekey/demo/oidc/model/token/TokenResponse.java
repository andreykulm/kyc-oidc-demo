package com.securekey.demo.oidc.model.token;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenResponse {

    private String accessToken;

    private String idToken;

    private String tokenType;

    private int expiresIn;

    private String jobId;

    private String clientTxnId;

    public String getAccessToken() {
        return accessToken;
    }

    @JsonProperty("access_token")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getIdToken() {
        return idToken;
    }

    @JsonProperty("id_token")
    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    @JsonProperty("token_type")
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    @JsonProperty("expires_in")
    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getJobId() {
        return jobId;
    }

    @JsonProperty("job_id")
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getClientTxnId() {
        return clientTxnId;
    }

    @JsonProperty("client_txn_id")
    public void setClientTxnId(String clientTxnId) {
        this.clientTxnId = clientTxnId;
    }

    @Override
    public String toString() {
        return "TokenResponse{" +
                "accessToken='" + accessToken + '\'' +
                ", idToken='" + idToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", expiresIn=" + expiresIn +
                ", jobId=" + jobId +
                ", client_txn_id =" + clientTxnId +
                '}';
    }
}
