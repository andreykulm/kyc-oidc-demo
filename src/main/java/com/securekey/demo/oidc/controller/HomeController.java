package com.securekey.demo.oidc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {
    @RequestMapping("/")
    ModelAndView showHomePage() {
        return new ModelAndView("index");
    }

    @RequestMapping("/baseFlow")
    ModelAndView showBaseFlowPage() {
        return new ModelAndView("base/base_index");
    }

    @RequestMapping("/extendedFlow")
    ModelAndView showExtendedFlowPage() {
        return new ModelAndView("extended/extended_index");
    }
}
