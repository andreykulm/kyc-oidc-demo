package com.securekey.demo.oidc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.securekey.demo.oidc.model.claims.GetJobClaimResponse;
import com.securekey.demo.oidc.model.get_job.GetJobResponse;
import com.securekey.demo.oidc.model.job.request.*;
import com.securekey.demo.oidc.model.job.response.CreateJobResponse;
import com.securekey.demo.oidc.model.token.TokenResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

@Controller
@RequestMapping(value = "/extended")
public class ExtendedClientController {

    private static final Logger logger = LoggerFactory.getLogger(ExtendedClientController.class);

    @Value("${client.url}")
    private String clientUrl;

    @Value("${dvs.url}")
    private String dvsUrl;

    @Value("${dvs.api.post.job}")
    private String dvsApiPostJob;

    @Value("${dvs.api.authorize}")
    private String dvsApiAuthorize;

    @Value("${dvs.api.get.job}")
    private String dvsApiGetJob;

    @Value("${dvs.api.token.exchange}")
    private String dvsApiTokenExchange;

    private List<String> authorizationCodesKeys = new ArrayList<>();
    private List<String> accessTokensKeys = new ArrayList<>();
    private List<String> permissionTokens = new ArrayList<>();
    private List<String> jobIds = new ArrayList<>();
    private List<String> userHrefs = new ArrayList<>();
    private List<String> clientIds = new ArrayList<>(); // 777
    private List<String> clientSecrets = new ArrayList<>(); // bearer 12345678

    @Autowired
    private RestTemplate restTemplate;

    // Redirects controller
    @RequestMapping("/createJobPage")
    ModelAndView showCreateJobPage() {
        return new ModelAndView("extended/create_job_request");
    }

    @RequestMapping("/authenticatePage")
    public ModelAndView showAuthenticatePage(RedirectAttributes attributes) throws JsonProcessingException {
        String userHref = userHrefs.get(0);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("extended/authenticate_request");
        modelAndView.addObject("userHref", userHref);
        return modelAndView;
    }

    @RequestMapping("/getJobByIdPage")
    public ModelAndView showGetJobByIdPage(RedirectAttributes attributes) {
        String jobId = jobIds.get(0);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("extended/get_job_by_id_request");
        modelAndView.addObject("jobId", jobIds.get(0));
        modelAndView.addObject("getJobRequest", dvsApiGetJob + jobId);
        return modelAndView;
    }

    @RequestMapping("/tokenPage")
    public ModelAndView showGetTokenPage(RedirectAttributes attributes) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("extended/exchange_token_request");
        modelAndView.addObject("code", authorizationCodesKeys.get(0));
        modelAndView.addObject("grant_type", "authorization_code");
        modelAndView.addObject("client_id", clientIds.get(0));
        return modelAndView;
    }

    @RequestMapping("/claimPage")
    public ModelAndView showClaimPage(RedirectAttributes attributes) {
        String requestUrl = dvsApiGetJob + jobIds.get(0) + "/claims";
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("extended/claims_request");
        modelAndView.addObject("requestUrl", requestUrl);
        modelAndView.addObject("jobId", jobIds.get(0));
        modelAndView.addObject("accessToken", accessTokensKeys.get(0));
        return modelAndView;
    }

    @RequestMapping(value = "/createJob", method = RequestMethod.POST)
    public ModelAndView createJob(HttpServletRequest request) throws JsonProcessingException {
        String clientSecret = request.getParameter("clientSecret");
        clientSecrets.clear();
        clientSecrets.add(clientSecret);

        String clientId = request.getParameter("clientTxnId");
        clientIds.clear();
        clientIds.add(clientId);

        CreateJobRequest createJobRequest = createJobRequestFromHttpRequest(request);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.AUTHORIZATION, "Bearer " + clientSecret);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<CreateJobRequest> createJobRequestHttpEntity = new HttpEntity<>(createJobRequest, httpHeaders);
        logger.info("\nCreate job request is " + createJobRequestHttpEntity);

        logger.info("Calling DVS at [" + dvsApiPostJob + "] with Headers: [" + httpHeaders.toString() + "] and Body: [" + createJobRequest + "].");
        CreateJobResponse createJobResponse = restTemplate.postForObject(dvsApiPostJob, createJobRequestHttpEntity, CreateJobResponse.class);
        logger.info("Job response is " + createJobResponse);

        // put permission token
        permissionTokens.clear(); // clear collection before new usage
        permissionTokens.add(createJobResponse.getPermissionToken());

        jobIds.clear(); // clear collection before new request
        jobIds.add(createJobResponse.getId()); // put job id

        userHrefs.clear();// clear collection before new usage
        userHrefs.add(createJobResponse.getUserHref()); // put userHref

//        ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        ObjectMapper objectMapper = new ObjectMapper();
        String createJobResponseJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(createJobResponse);
        logger.info("Create job response json is " + createJobResponseJson);

        ModelAndView modelAndView = new ModelAndView("extended/create_job_response");
        modelAndView.addObject("jobResponse", createJobResponse);
        modelAndView.addObject("jobResponseJson", createJobResponseJson);
        return modelAndView;
    }

    @RequestMapping("/authenticate")
    public ModelAndView redirectWithUsingRedirectView() throws UnsupportedEncodingException, JsonProcessingException {
        List<NameValuePair> uriParams = new ArrayList<>();
        try {
            uriParams = URLEncodedUtils.parse(new URI(userHrefs.get(0)), UTF_8);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to parse userHref: ", e);
        }

        // get parameters from user href
        MultiValueMap<String, String> formVars = formVarsFromUriParams(uriParams);
//        Map<String, String> formVars = formVarsFromUriParams(uriParams);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        httpHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        logger.info("Request parameters to dvs service: " + formVars.toString());

        HttpEntity<MultiValueMap<String, String>> authorizeRequest = new HttpEntity<>(formVars, httpHeaders);
//        HttpEntity<String> authorizeRequest = new HttpEntity<>(httpHeaders);
        logger.info("Calling DVS at [" + dvsApiAuthorize + "] with Headers: [" + httpHeaders.toString() + "] and Body: [" + authorizeRequest + "].");

        ResponseEntity<String> authorizeResponse = restTemplate.postForEntity(dvsApiAuthorize, authorizeRequest, String.class);

//        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(dvsApiAuthorize);
//        for (Map.Entry<String, String> entry : formVars.entrySet()) {
//            builder.queryParam(entry.getKey(), entry.getValue());
//            logger.info("Adding parameter " + entry.getKey() + " with value " + entry.getValue());
//        }

//        String authorizeUrl = builder.toUriString();
//        logger.info("Authorize url with parameters is " + authorizeUrl);

//        ResponseEntity<String> authorizeResponse = restTemplate.exchange(authorizeUrl, HttpMethod.GET, authorizeRequest, String.class);
        logger.info("Response from authorize endpoint is: " + authorizeResponse.getBody() + "with headers " + authorizeResponse.getHeaders());

        String locationUrl = authorizeResponse.getHeaders().get("Location").get(0);
        logger.info("Location header is " + locationUrl);

        MultiValueMap<String, String> allParameters = UriComponentsBuilder.fromUriString(locationUrl).build().getQueryParams();
        String redirectUri = allParameters.get("redirectUri").get(0);
        logger.info("RedirectUri is " + redirectUri);

        String decodedRedirectUri = URLDecoder.decode(redirectUri, StandardCharsets.UTF_8.toString());
        logger.info("Decoded redirect uri is " + decodedRedirectUri);

        MultiValueMap<String, String> redirectUriParameters = UriComponentsBuilder.fromHttpUrl(decodedRedirectUri).build().getQueryParams();
        String code = redirectUriParameters.get("code").get(0);
        authorizationCodesKeys.clear(); // clear the collection before new usage
        authorizationCodesKeys.add(code); // add one code value to retrieve it in token endpoint

        ObjectMapper objectMapper = new ObjectMapper();
        String authResponseJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(authorizeResponse);
        logger.info("Auth response json is " + authResponseJson);

        ModelAndView modelAndView = new ModelAndView("extended/auth_response");
        modelAndView.addObject("locationUrl", locationUrl);
        modelAndView.addObject("code", code);
        modelAndView.addObject("authResponseJson", authResponseJson);
        return modelAndView;
    }

    @GetMapping(value = "/getJob/{jobId}")
    public ModelAndView getJob(@PathVariable("jobId") String jobId) {
        logger.info("Job id is " + jobId);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "bearer " + clientSecrets.get(0));

        logger.info("Get job url is " + dvsApiGetJob.concat(jobId));
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<GetJobResponse> jobResponse = restTemplate.exchange(dvsApiGetJob.concat(jobId), HttpMethod.GET, entity, GetJobResponse.class);

        GetJobResponse responseBody = jobResponse.getBody();
        logger.info("Get job response is " + responseBody);

        ModelAndView modelAndView = new ModelAndView("extended/get_job_response");
        modelAndView.addObject("jobResponse", responseBody);
        return modelAndView;
    }

    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ModelAndView exchangeToken(HttpServletRequest httpServletRequest) {
        MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
        formVars.add("code", httpServletRequest.getParameter("code"));
        formVars.add("grant_type", httpServletRequest.getParameter("grant_type"));
        formVars.add("redirect_uri", httpServletRequest.getParameter("redirect_uri"));
        formVars.add("client_id", httpServletRequest.getParameter("client_id"));
        logger.info("Token request parameters: " + formVars.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.AUTHORIZATION, "bearer " + clientSecrets.get(0));

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(formVars, headers);
        TokenResponse tokenResponse = restTemplate.postForObject(dvsApiTokenExchange, request, TokenResponse.class);
        logger.info("Token response is " + tokenResponse.toString());

        ModelAndView modelAndView = new ModelAndView("extended/token_response");
        modelAndView.addObject("token", tokenResponse);
        accessTokensKeys.clear(); // clear the collection before new usage
        accessTokensKeys.add(tokenResponse.getAccessToken()); // add one code value to retrieve it in claims endpoint
        return modelAndView;
    }

    @RequestMapping(value = "/{jobId}/claims", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json")
    public ModelAndView userInfo(@PathVariable(value = "jobId") String jobId) {
        logger.info("Job id is " + jobId);
        MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add(HttpHeaders.ACCEPT, "application/json");
        headers.add(HttpHeaders.AUTHORIZATION, "bearer " + accessTokensKeys.get(0));
        logger.info("Client secret is " + accessTokensKeys.get(0));

        HttpEntity<MultiValueMap<String, String>> claimsRequest = new HttpEntity<>(formVars, headers);
        logger.info("Claims request " + claimsRequest.toString());

        String getJobClaimUrl = dvsApiGetJob.concat(jobId).concat("/claims");
        logger.info("Get job claim url is " + getJobClaimUrl);

        ResponseEntity<GetJobClaimResponse> jobClaimResponse = restTemplate.exchange(getJobClaimUrl, HttpMethod.GET, claimsRequest, GetJobClaimResponse.class);
        logger.info("Get jobs claims response is " + jobClaimResponse);

        GetJobClaimResponse getJobClaimResponse = jobClaimResponse.getBody();

        ModelAndView modelAndView = new ModelAndView("extended/claims_response");
        modelAndView.addObject("jobClaimResponse", getJobClaimResponse);
        return modelAndView;
    }


    private CreateJobRequest createJobRequestFromHttpRequest(HttpServletRequest request) {
        // Form subject
        AddressDto address = new AddressDto();
        address.setStreetAddress(request.getParameter("streetAddress"));
        address.setLocality(request.getParameter("locality"));
        address.setRegion(request.getParameter("region"));
        address.setPostalCode(request.getParameter("postalCode"));
        address.setCountry(request.getParameter("country"));

        Subject subject = new Subject();
        subject.setGivenName(request.getParameter("givenName"));
        subject.setMiddleName(request.getParameter("middleName"));
        subject.setFamilyName(request.getParameter("familyName"));
        subject.setBirthdate(request.getParameter("phoneNumber"));
        subject.setAddress(address);
        subject.setPhoneNumber(request.getParameter("phoneNumber"));
        subject.setEmail(request.getParameter("email"));


        // set job verify portion
        JobVerifyCallbackDto callback = new JobVerifyCallbackDto();
        callback.setReturnUri(request.getParameter("returnUri"));
        callback.setState(request.getParameter("state"));

        JobVerify verify = new JobVerify();
        verify.setTier(request.getParameter("tier"));
        verify.setLocale(request.getParameter("locale"));
        verify.setClientTxnId(request.getParameter("clientTxnId"));
        verify.setSessionExp(request.getParameter("sessionExp"));
        verify.setCallback(callback);

        CreateJobRequest createJobRequest = new CreateJobRequest();
        createJobRequest.setSubject(subject);
        createJobRequest.setVerify(verify);

        return createJobRequest;
    }

    private MultiValueMap<String, String> formVarsFromUriParams(List<NameValuePair> uriParams) {
        MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
        uriParams.forEach(val -> {
            formVars.add(val.getName(), val.getValue());
        });
        return formVars;
    }

//    private Map<String, String> formVarsFromUriParams(List<NameValuePair> uriParams) {
//        Map<String, String> formVars = new HashMap<>();
//        uriParams.forEach(val -> formVars.put(val.getName(), val.getValue()));
//        return formVars;
//    }

}
