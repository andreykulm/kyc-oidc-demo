package com.securekey.demo.oidc.controller;

import com.securekey.demo.oidc.model.claims.GetJobClaimResponse;
import com.securekey.demo.oidc.model.get_job.GetJobResponse;
import com.securekey.demo.oidc.model.job.request.*;
import com.securekey.demo.oidc.model.job.response.CreateJobResponse;
import com.securekey.demo.oidc.model.token.TokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/base")
public class ClientController {

    private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    @Value("${client.url}")
    private String clientUrl;

    @Value("${dvs.url}")
    private String dvsUrl;

    @Value("${dvs.client_id}")
    private String clientId;

    @Value("${dvs.client_api_key}")
    private String clientApiKey;

    @Autowired()
    private RestTemplate restTemplate;

    private List<String> authorizationCodesKeys = new ArrayList<>();
    private List<String> accessTokensKeys = new ArrayList<>();
    private List<String> permissionTokens = new ArrayList<>();
    private List<String> jobIds = new ArrayList<>();

    @RequestMapping(value = "/createJob", method = RequestMethod.POST)
    public ModelAndView createJob() {
        // set subject portion
        AddressDto address = new AddressDto();
        address.setStreetAddress("12-123 Main St. E");
        address.setLocality("Toronto");
        address.setRegion("ON");
        address.setPostalCode("A2A 3G4");
        address.setCountry("CA");

        Subject subject = new Subject();
        subject.setGivenName("Alexander");
        subject.setMiddleName("Peter");
        subject.setFamilyName("Petrovski");
        subject.setBirthdate("1984-03-07");
        subject.setAddress(address);
        subject.setPhoneNumber("+14165550666");
        subject.setEmail("test123@gmail.com");

        // set job verify portion
        JobVerifyCallbackDto callback = new JobVerifyCallbackDto();
        callback.setReturnUri(clientUrl);
        callback.setState("999");

        JobVerify verify = new JobVerify();
        verify.setTier("1");
        verify.setLocale("en_CA");
        verify.setClientTxnId("777");
        verify.setSessionExp("600");
        verify.setCallback(callback);

        CreateJobRequest createJobRequest = new CreateJobRequest();
        createJobRequest.setSubject(subject);
        createJobRequest.setVerify(verify);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.AUTHORIZATION, "Bearer " + clientApiKey);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<CreateJobRequest> createJobRequestHttpEntity = new HttpEntity<>(createJobRequest, httpHeaders);
        logger.info("\nCreate job request is " + createJobRequestHttpEntity);

        String postJobUrl = dvsUrl + "/dev/job";

        logger.info("Calling DVS at [" + postJobUrl + "] with Headers: [" + httpHeaders.toString() + "] and Body: [" + createJobRequest + "].");

        CreateJobResponse createJobResponse = restTemplate.postForObject(postJobUrl, createJobRequestHttpEntity, CreateJobResponse.class);
        logger.info("Job response is " + createJobResponse);

        permissionTokens.clear(); // clear collection before new usage
        permissionTokens.add(createJobResponse.getPermissionToken()); // put permission token

        jobIds.clear(); // clear collection before new request
        jobIds.add(createJobResponse.getId()); // put job id

        ModelAndView modelAndView = new ModelAndView("base/create_job_response");
        modelAndView.addObject("jobResponse", createJobResponse);
        return modelAndView;
    }

    @RequestMapping("/authenticate")
    public ModelAndView redirectWithUsingRedirectView(RedirectAttributes attributes) throws UnsupportedEncodingException {
        MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
        formVars.add("response_type", "code");
        formVars.add("client_id", clientId);
        formVars.add("scope", "openid tier1");
        formVars.add("redirect_uri", clientUrl);
        formVars.add("state", "999");
        formVars.add("ptok", permissionTokens.get(0));
        formVars.add("job_id", jobIds.get(0));
        logger.info("Authorize request to dac adapter: " + formVars.toString());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        httpHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<MultiValueMap<String, String>> authorizeRequest = new HttpEntity<>(formVars, httpHeaders);

        String authorizeUrl = dvsUrl + "/dev/auth/request";

        logger.info("Calling DVS at [" + authorizeUrl + "] with Headers: [" + httpHeaders.toString() + "] and Body: [" + authorizeRequest + "].");

        ResponseEntity<String> authorizeResponse = restTemplate.postForEntity(authorizeUrl, authorizeRequest, String.class);
        logger.info("Response from authorize endpoint is: " + authorizeResponse.getBody() + "with headers " + authorizeResponse.getHeaders());

        String locationUrl = authorizeResponse.getHeaders().get("Location").get(0);
        logger.info("Location header is " + locationUrl);

        MultiValueMap<String, String> allParameters = UriComponentsBuilder.fromUriString(locationUrl).build().getQueryParams();
        String redirectUri = allParameters.get("redirectUri").get(0);
        logger.info("RedirectUri is " + redirectUri);

        String decodedRedirectUri = URLDecoder.decode(redirectUri, StandardCharsets.UTF_8.toString());
        logger.info("Decoded redirect uri is " + decodedRedirectUri);

        MultiValueMap<String, String> redirectUriParameters = UriComponentsBuilder.fromHttpUrl(decodedRedirectUri).build().getQueryParams();
        String code = redirectUriParameters.get("code").get(0);
        authorizationCodesKeys.clear(); // clear the collection before new usage
        authorizationCodesKeys.add(code); // add one code value to retrieve it in token endpoint

        ModelAndView modelAndView = new ModelAndView("base/auth_response");
        modelAndView.addObject("locationUrl", locationUrl);
        modelAndView.addObject("code", code);
        return modelAndView;
    }

    @RequestMapping(value = "/getJob")
    public ModelAndView getJob() {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + clientApiKey);

        String jobId = jobIds.get(0);
        logger.info("Job id is " + jobId);

        String getJobUrl = dvsUrl + "/dev/job/".concat(jobId);
        logger.info("Get job url is " + getJobUrl);

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<GetJobResponse> jobResponse = restTemplate.exchange(getJobUrl, HttpMethod.GET, entity, GetJobResponse.class);

        GetJobResponse responseBody = jobResponse.getBody();
        logger.info("Get job response is " + responseBody);

        ModelAndView modelAndView = new ModelAndView("base/get_job_response");
        modelAndView.addObject("jobResponse", responseBody);
        return modelAndView;
    }

    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ModelAndView exchangeToken() {
        MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();
        formVars.add("code", authorizationCodesKeys.get(0));
        formVars.add("grant_type","authorization_code");
        formVars.add("redirect_uri",clientUrl);
        formVars.add("client_id",clientId);
        logger.info("Token request parameters: " + formVars.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + clientApiKey);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(formVars, headers);
        TokenResponse tokenResponse = restTemplate.postForObject(dvsUrl + "/dev/token/exchange", request, TokenResponse.class);
        logger.info("Token response is " + tokenResponse.toString());

        ModelAndView modelAndView = new ModelAndView("base/token_response");
        modelAndView.addObject("token", tokenResponse);
        accessTokensKeys.clear(); // clear the collection before new usage
        accessTokensKeys.add(tokenResponse.getAccessToken()); // add one code value to retrieve it in claims endpoint
        return modelAndView;
    }

    @RequestMapping(value = "/claims", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ModelAndView userInfo() {
        MultiValueMap<String, String> formVars = new LinkedMultiValueMap<>();

        String jobId = jobIds.get(0);
        logger.info("Job id is " + jobId);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + accessTokensKeys.get(0));
        logger.info("Client secret is " + accessTokensKeys.get(0));

        HttpEntity<MultiValueMap<String, String>> claimsRequest = new HttpEntity<>(formVars, headers);
        logger.info("Claims request " + claimsRequest.toString());

        String getJobClaimUrl = dvsUrl + "/dev/job/".concat(jobId).concat("/claims");
        logger.info("Get job claim url is " + getJobClaimUrl);

        ResponseEntity<GetJobClaimResponse> jobClaimResponse = restTemplate.exchange(getJobClaimUrl, HttpMethod.GET, claimsRequest, GetJobClaimResponse.class);
        logger.info("Get jobs claims response is " + jobClaimResponse);

        GetJobClaimResponse getJobClaimResponse = jobClaimResponse.getBody();

        ModelAndView modelAndView = new ModelAndView("base/claims_response");
        modelAndView.addObject("jobClaimResponse", getJobClaimResponse);
        return modelAndView;
    }

}
