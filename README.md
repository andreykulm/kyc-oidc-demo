Synopsis
The project represents a client service that interacts with appropriate OIDC authrorization server.

Installation
1) Run 'mvn clean package' in the root directory.
2) Take the generate .war file from the 'target' folder and run it like 'java -jar {filename}.war'.
2.1) As a alternative run the project in you IDE.  
3) Press the buttons on the ui page to get the required response from the OIDC server.